package com.fit3140.superwidgets.interpreter.operators;

import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeMap;

import com.fit3140.superwidgets.interpreter.exceptions.LineNumberDoesNotExistException;
import com.fit3140.superwidgets.interpreter.exceptions.MyParseException;

/**
 * stores the compiled "bytecode" of a DUMBBASIC program.  
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class OpCollection {
	private TreeMap<Integer, Op> ops;
	private Integer firstop = null;
	
	/**
	 * Create the OpCollection from an input stream giving us DUMBBASIC source
	 * 
	 * @param sr
	 * @throws MyParseException
	 */
	public OpCollection(Scanner sr) throws MyParseException {
		OpMaker opm = new OpMaker();
		ops = new TreeMap<Integer, Op>();
		while(sr.hasNextLine()) {
			String nextline = sr.nextLine();
			Op nextop = opm.make(nextline);
			ops.put(nextop.getLineNo(), nextop);
		}
		addNextLns();
		Iterator <Integer>linelist=ops.keySet().iterator();
		if (linelist.hasNext()) {
			firstop = linelist.next();
		}
		return;
	}

	/**
	 * get the Op corresponding to a given line number
	 * @param lineno
	 * @return
	 * @throws LineNumberDoesNotExistException
	 */
	public Op getOp(Integer lineno) throws LineNumberDoesNotExistException {
		Op thisOp = ops.get(lineno);
		if (thisOp == null) {
			throw new LineNumberDoesNotExistException();
		}
		return thisOp;
	}
	
	/**
	 * Get the first (lowest) line number in this DUMBBASIC program
	 * @return
	 */
	public Integer getFirstLineno() {
		return firstop;
	}
	
	// add the "next line" values to each Op in the collection
	// we can only do this when complete because we can put things into a DUMBBASIC program
	// in any order - the numerical order of the line numbers is what matters.
	
	// this is something of a direct port from the Python version - there may be more 
	// efficient algorithms.
	
	private void addNextLns() {
		Iterator<Op> oplist = ops.values().iterator();
		if (!(oplist.hasNext())) {
			return;
		}
		Iterator<Integer> linenext = ops.keySet().iterator();
		linenext.next();
		while(linenext.hasNext()) {
			Op op = oplist.next();
			int lineno = linenext.next();
			op.setNextLn(lineno);
		}
	}
}
