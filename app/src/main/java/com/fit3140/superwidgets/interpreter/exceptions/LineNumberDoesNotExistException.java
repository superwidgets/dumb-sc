package com.fit3140.superwidgets.interpreter.exceptions;

/**
 * Exception to invoke if a GOTO or IF-GOTO tries to GOTO a non-existent line number
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class LineNumberDoesNotExistException extends Exception {

	public LineNumberDoesNotExistException() {
		// TODO Auto-generated constructor stub
	}

	public LineNumberDoesNotExistException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public LineNumberDoesNotExistException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public LineNumberDoesNotExistException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public LineNumberDoesNotExistException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		//super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}
