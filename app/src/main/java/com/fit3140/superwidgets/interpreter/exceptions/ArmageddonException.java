package com.fit3140.superwidgets.interpreter.exceptions;

/**
 * Thrown when the structure of a DUMBBASIC statement does not match the permitted forms.
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class ArmageddonException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 3334936271124777769L;

	public ArmageddonException() {
	}

	public ArmageddonException(String message) {
		super(message + "\nIt's the end of the computational world. There's nowhere left to go!");
	}

	public ArmageddonException(Throwable cause) {
		super(cause);
	}

	public ArmageddonException(String message, Throwable cause) {
		super(message, cause);
	}

	public ArmageddonException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		//super(message, cause, enableSuppression, writableStackTrace);
	}

}
