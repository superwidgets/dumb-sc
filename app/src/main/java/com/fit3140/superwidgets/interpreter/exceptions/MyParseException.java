package com.fit3140.superwidgets.interpreter.exceptions;

/**
 * Thrown when the structure of a DUMBBASIC statement does not match the permitted forms.
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class MyParseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3334936271124777769L;

	public MyParseException() {
	}

	public MyParseException(String message) {
		super(message);
	}

	public MyParseException(Throwable cause) {
		super(cause);
	}

	public MyParseException(String message, Throwable cause) {
		super(message, cause);
	}

	public MyParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		//super(message, cause, enableSuppression, writableStackTrace);
	}

}
