package com.fit3140.superwidgets.interpreter.expressions;

import com.fit3140.superwidgets.interpreter.Context;
import com.fit3140.superwidgets.interpreter.exceptions.ValueNotPresentException;

public class Equals extends Expression {

	public Equals(String left, String right) {
		super(left, right);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int evaluate(Context context) throws ValueNotPresentException {
		int leftval = context.getValue(lhs);
		int rightval = context.getValue(rhs);
		
	
		if (leftval == rightval) {
			return 1;
		}else {
			return 0;
		}
	}
}
