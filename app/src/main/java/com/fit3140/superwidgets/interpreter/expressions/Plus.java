package com.fit3140.superwidgets.interpreter.expressions;

import com.fit3140.superwidgets.interpreter.Context;
import com.fit3140.superwidgets.interpreter.exceptions.ValueNotPresentException;

/**
 * Implements an addition expression
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class Plus extends Expression {

	public Plus(String left, String right) {
		super(left, right);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int evaluate(Context context) throws ValueNotPresentException {
		int leftval = context.getValue(lhs);
		int rightval = context.getValue(rhs);
		return leftval + rightval;
	}
}
