package com.fit3140.superwidgets.interpreter.operators;

import com.fit3140.superwidgets.interpreter.Context;
import com.fit3140.superwidgets.interpreter.exceptions.ArmageddonException;

/**
 * Created by james on 5/18/2016.
 */
public class ReturnSub extends Op {

    /**
     * create a GOTO op
     *
     * @param line - the line number of *this* op
     *
     */
    public ReturnSub(int line) {
        super(line);

        return;
    }

    @Override
    public String execute(Context context) throws Exception {
        try {
            context.popFromStack();
            return "";
        }
        catch (Exception e) {
            throw new ArmageddonException("Invalid operator 'RETURN': call stack is empty!\n" + e.toString() + e.getMessage());
        }


    }
}