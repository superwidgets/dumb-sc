package com.fit3140.superwidgets.interpreter.operators;

import com.fit3140.superwidgets.interpreter.Context;
import com.fit3140.superwidgets.interpreter.expressions.Expression;

/**
 * implements the IF (expression) GOTO (target) statement
 * @author Robert Merkel <robert.merkel@monash.edu>
 * @Modified_By Timothy Laird
 * @Last_Modified 08/05/2016
 */
public class IfGoto extends Op {
	private Expression expression;
	private String target;
	/**
	 * create an IF-GOTO
	 * 
	 * @param line - the current line number
	 * @param exp - the expression that must evaluate to true (non-zero) for the GOTO to occur
	 * @param tgt - where to go if expression is true.  can be a numeric literal or a variable name
	 */
	public IfGoto(int line, Expression exp, String tgt) {
		super(line);
		expression=exp;
		target = tgt;
		return;
	}

	@Override
	public String execute(Context context) throws Exception {
		try {
			int expval = expression.evaluate(context);
			if (expval != 0) {
				int targetval = context.getValue(target);
				context.setIp(targetval);
			} else {
				context.setIp(nextln);
			}
			return "";
		}
		catch (Exception e) {
			throw new Exception("IfGoto failed to execute");
		}
	}
}
