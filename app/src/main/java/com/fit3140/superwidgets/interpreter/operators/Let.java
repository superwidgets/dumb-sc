package com.fit3140.superwidgets.interpreter.operators;

import com.fit3140.superwidgets.interpreter.Context;
import com.fit3140.superwidgets.interpreter.expressions.Expression;

/**
 * Implements the LET operation
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 * @Modified_By Timothy Laird
 * @Last_Modified 08/05/2016
 */
public class Let extends Op {

	private String var; // name of the variable whose value is set in this operation
	private Expression exp; // the expression to evaluate 
	private String val;
	
	/**
	 * create a LET op
	 * @param line - line number of this op
	 * @param varname - varname to set
	 * @param expression - expression to evaluate when executed
	 */
	
	public Let(int line, String varname, Expression expression) {
		super(line);
		var=varname;
		exp=expression;
		val=null;
		return;
	}
	
	public Let(int line, String varname, String value) {
		super(line);
		var=varname;
		exp=null;
		val=value;
		return;
	}

	@Override
	public String execute(Context context) throws Exception {
		try {
			if (!(exp == null)) {
				context.setValue(var, exp.evaluate(context));
			} else {
				context.setString(var, val);
			}
			context.setIp(nextln);
			return "";
		}
		catch (Exception e) {
			throw new Exception("Let failed to execute");
		}
	}
}
