package com.fit3140.superwidgets.interpreter.expressions;

import com.fit3140.superwidgets.interpreter.Context;
import com.fit3140.superwidgets.interpreter.exceptions.ValueNotPresentException;

/**
 * implements a subtraction expression
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class Minus extends Expression {

	public Minus(String left, String right) {
		super(left, right);
	}

	@Override
	public int evaluate(Context context) throws ValueNotPresentException {
		int leftval = context.getValue(lhs);
		int rightval = context.getValue(rhs);
		return leftval - rightval;
	}
}
