package com.fit3140.superwidgets.interpreter;

import java.util.Scanner;

import com.fit3140.superwidgets.interpreter.exceptions.MyParseException;
import com.fit3140.superwidgets.interpreter.operators.Op;
import com.fit3140.superwidgets.interpreter.operators.OpCollection;

/**
 * DumbBasic Interpreter class, used to load and run DumbBasic programs
 * 
 * @Author Robert Merkel <robert.merkel@monash.edu>
 * @Modified_By Timothy Laird
 * @Last_Modified 12/05/2012
 *
 */
public class Interpreter {

	private Scanner program; // The program which will be run
    private String output; // Field to store the output of the program

    /**
     * Create an Interpreter instance
     *
     */
	public Interpreter() {
		// TODO Auto-generated constructor stub
	}

    /**
     * Create an Interpreter instance and load it with a program
     *
     * @param code, program string to be loaded
     *
     */
    public Interpreter(String code) {
        program = new Scanner(code);
        output = "";
    }

    /**
     * Get the output of the last run
     *
     * @return String
     *
     */
    public String getOutput() {
        return output;
    }

    /**
     * Stores a program
     *
     * @param code, program string to be loaded
     *
     */
    public boolean load(String code) {
        try {
            program = new Scanner(code);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * Gets the currently stored program
     *
     * @return string
     *
     */
    public String read() {
        return program.toString();
    }
	/**
	 * Runs the loaded DumbBasic program. If any Exception is thrown while running the program, the
     * exception message will be added to the output and the program stopped
     *
     * @return String
	 * @throws MyParseException
	 */
	public String run() throws MyParseException {
        if (program == null) {
            throw new MyParseException("No program is loaded");
        }
		try {
			OpCollection ops = new OpCollection(program);
			Context context = new Context();
            output = "";
            String out;

			context.setIp(ops.getFirstLineno());
		
			while(context.getIp() != null) {
				Op thisOp = ops.getOp(context.getIp());
				out = thisOp.execute(context);
                if (!out.equals("")) {
                    output += out + "\n";
                }
			}
		}
        catch (Exception e) {
            output += e.toString() + "\n";
			//output += e.getMessage();
		}
        return output;
	}
}
