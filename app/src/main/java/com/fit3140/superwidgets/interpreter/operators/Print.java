package com.fit3140.superwidgets.interpreter.operators;

import com.fit3140.superwidgets.interpreter.Context;

/**
 * Print a constant, or the value of a variable, to console
 * @author Robert Merkel <robert.merkel@monash.edu>
 * @Modified_By Timothy Laird
 * @Last_Modified 08/05/2016
 */
public class Print extends Op {

	private String value;
	public Print(int line, String val) {
		super(line);
		value = val;
	}

	@Override
	public String execute(Context context) throws Exception {
		try {
			context.setIp(getNextLn());
			try {
				return Integer.toString(context.getValue(value));
			} catch (Exception e) {
				return context.getString(value);
			}
		}
		catch (Exception e) {
			throw new Exception("Print failed to execute");
		}
	}
}
