package com.fit3140.superwidgets.interpreter.operators;

import android.text.TextUtils;

import java.util.Arrays;

import com.fit3140.superwidgets.interpreter.exceptions.MyParseException;
import com.fit3140.superwidgets.interpreter.expressions.Expression;
import com.fit3140.superwidgets.interpreter.expressions.ExpressionMaker;

/**
 * turns a line of DUMBBASIC into an op
 * 
 * Please note that adding a new Op to the language will require 
 * adding code to this class.
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 * @Modified_By Timothy Laird
 * @Last_Modified 08/05/2016
 *
 */
public class OpMaker {

	private ExpressionMaker exp_maker;
	public OpMaker() {
		 exp_maker=new ExpressionMaker();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Turns a line of text into an Op.  Add to this method if you
	 * add a new Op 
	 * @param ops
	 * @return
	 * @throws MyParseException
	 */
	public Op make(String ops) throws MyParseException {
		String []tokens = ops.split("\\s+"); // Split on whitespace
		if (tokens.length <= 1) {
			throw new MyParseException("OpMaker: Invalid number of arguments:\n" + Arrays.toString(tokens));
		}
		int lineNo;
		try {
			lineNo = Integer.parseInt(tokens[0]);
		} catch (NumberFormatException e) {
			throw new MyParseException("OpMaker: Failed to parse line number\n" + e.getMessage());
		}
		
		switch (tokens[1].toUpperCase()) {
		case "REM":
			return new Remark(lineNo);
		case "PRINT":
			try {
				return new Print(lineNo, TextUtils.join(" ", Arrays.copyOfRange(tokens, 2, tokens.length)));
			} catch (Exception e) {
				throw new MyParseException("OpMaker: Failed to create PRINT op\n" + e.getMessage());
			}
		case "GOTO": //LINENO_0 GOTO_1 TARGET_2
			try {
				return new Goto(lineNo, tokens[2]);
				
			} catch (Exception e) {
				throw new MyParseException("OpMaker: Failed to create GOTO op\n" + e.getMessage());
			}
		case "LET": // LINENO_0 LET_1 VARIABLE_2 =_3 LHS_4 OP_5 RHS_6
			String variable = tokens[2];
			Expression letExp;
			if (variable.endsWith("$")) { // If the expression is just a string, join all the tokens
				return new Let(lineNo, variable, TextUtils.join(" ", Arrays.copyOfRange(tokens, 4, tokens.length)));
			} 
			else {
				if (tokens.length == 5) { // If the expression is just one value
                    letExp = exp_maker.makeExpression(tokens[4]);
				}
				else if (tokens.length == 7) { // If the expression contains an operator
                    letExp = exp_maker.makeExpression(tokens[4], tokens[5], tokens[6]);
				}
				else {
					throw new MyParseException("OpMaker: Failed to create LET op");
				}
			}
			return new Let(lineNo, variable, letExp);
		case "IF": // LINENO_) IF_1 LHS_2 OP_3 RHS_4 GOTO_5 TARGET_6
			try {
				Expression ifGoToExp = exp_maker.makeExpression(tokens[2], tokens[3], tokens[4]);
				return new IfGoto(lineNo, ifGoToExp, tokens[6]);
			} catch (Exception e) {
				throw new MyParseException("OpMaker: Failed to create IF op\n" + e.getMessage());
			}
        case "GOSUB": //LINENO_0 GOTO_1 TARGET_2
            try {
                return new Gosub(lineNo, tokens[2]);

            } catch (Exception e) {
                throw new MyParseException("OpMaker: Failed to create GOSUB op\n" + e.getMessage());
            }
        case "RETURN": //LINENO_0 GOTO_1 TARGET_2
            try {
                return new ReturnSub(lineNo);

            } catch (Exception e) {
                throw new MyParseException("OpMaker: Failed to create RETURN op\n" + e.getMessage());
            }
		default:
			throw new MyParseException();
		}
	}
}
  