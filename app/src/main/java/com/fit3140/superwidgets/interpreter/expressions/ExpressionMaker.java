package com.fit3140.superwidgets.interpreter.expressions;

import com.fit3140.superwidgets.interpreter.exceptions.MyParseException;

/**
 * A parser for expressions that creates one of the right type depending on the infix operator
 * specified.
 * 
 * Adding another Expression subclasses will require you to edit this class.
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 * @Modified_By Timothy Laird
 * @Last_Modified 08/05/2016
 */
public class ExpressionMaker {

	/**
	 * constructor
	 */
	public ExpressionMaker() {
		return;
	}
	
	/**
	 * Make an expression of the appropriate type
	 * 
	 * @param lhs - the left hand side of the expression
	 * @param op - the operator symbol
	 * @param rhs - the right hand side of the expression.
	 * @return
	 * @throws MyParseException
	 */
	public Expression makeExpression(String lhs, String op, String rhs) throws MyParseException {
		switch (op ) {
		case "+":
			return new Plus(lhs, rhs);
		case "-":
			return new Minus(lhs, rhs);
		case "==":
			return new Equals(lhs, rhs);
		case ">":
			return new GreaterThan(lhs, rhs);
		default:
			throw new MyParseException("unrecognized expression operator");
		}
	}

	/**
	 * Make an expression which consists of only 1 value
	 *
	 * @param value - value.
	 * @return
	 * @throws MyParseException
	 */
	public Expression makeExpression(String value) throws MyParseException {
		return new IntValue(value);
	}
}
