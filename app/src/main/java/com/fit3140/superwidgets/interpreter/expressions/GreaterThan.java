package com.fit3140.superwidgets.interpreter.expressions;

import com.fit3140.superwidgets.interpreter.Context;
import com.fit3140.superwidgets.interpreter.exceptions.ValueNotPresentException;

/**
 * Implements the "greater-than" (>) logical operation.
 * 
 * By convention, in DUMBBASIC logical operators return 1 if true, 0 otherwise.  
 *
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class GreaterThan extends Expression {

	public GreaterThan(String left, String right) {
		super(left, right);
	}

	@Override
	public int evaluate(Context context) throws ValueNotPresentException {
		int leftval = context.getValue(lhs);
		int rightval = context.getValue(rhs);
		
		if (leftval > rightval) {
			return 1;
		}else {
			return 0;
		}
	}
}
