package com.fit3140.superwidgets.interpreter.operators;

import com.fit3140.superwidgets.interpreter.Context;

/**
 * implements the GOTO operation
 * @author Robert Merkel <robert.merkel@monash.edu>
 * @Modified_By Timothy Laird
 * @Last_Modified 08/05/2016
 */
public class Goto extends Op {

	private String target; // either a string representing a numeric constant, or a variable name
	/**
	 * create a GOTO op
	 * @param line - the line number of *this* op
	 * @param targ - either a string literal of a number, or a variable name, which represents where we should GO TO
	 */
	public Goto(int line, String targ) {
		super(line); 
		target=targ;
		return;
	}

	@Override
	public String execute(Context context) throws Exception {
		try {
			int newline = context.getValue(target);
			context.setIp(newline);
			return "";
		}
		catch (Exception e) {
			throw new Exception("Goto failed to execute");
		}
	}
}
