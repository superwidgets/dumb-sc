package com.fit3140.superwidgets.interpreter;

import android.util.Log;

import java.util.ArrayList;
import java.util.TreeMap;

import com.fit3140.superwidgets.interpreter.exceptions.MyParseException;
import com.fit3140.superwidgets.interpreter.exceptions.ValueNotPresentException;

/**
 * Stores the state of the running DUMBBASIC program, including variable values and instruction pointer
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 * @Modified_By Timothy Laird
 * @Last_Modified 08/05/2016
 * 
 */
public class Context {
	public ArrayList<Integer> callstack;
	private Integer ip;
	private TreeMap<String, Integer> int_table; 
	private TreeMap<String, String> string_table; 

	public Context() {
		callstack = new ArrayList<>();
		ip = null;
		int_table = new TreeMap<String, Integer>();
		string_table = new TreeMap<String, String>();
	}
	/**
	 * Get the value of this expression component, which should a string literal containing an integer or a variable name
	 * @param varname 
	 * @return an integer representing the value of either the literal or the variable
	 * @throws ValueNotPresentException
	 */
	public int getValue(String varname) throws ValueNotPresentException {
		int val;
		if (int_table.containsKey(varname)) {
				val= int_table.get(varname);
		}
		else {
			try {
				val = Integer.parseInt(varname);
			} 
			catch(NumberFormatException e) {
			throw new ValueNotPresentException();
			}	
		}
		return val;
	}
	
	/**
	 * Get the value of this expression component, which should a string literal or a variable name
	 * @param varname, name of the variable
	 * @return an string representing the value of either the literal or the variable
	 * @throws ValueNotPresentException
	 */
	public String getString(String varname) throws ValueNotPresentException {
		String val;
		if (string_table.containsKey(varname)) {
			val = string_table.get(varname);
		}		
		else {
			val = varname;
		}
		return val;
	}
	
	/**
	 * Set the value of a variable to an int
	 * @param variable - the name of the variable
	 * @param value - the integer value to set it to
	 */
	public void setValue(String variable, int value) {
		int_table.put(variable, value);
		return;
	}

    /**
     * Set the value of a variable to a string
     * @param variable - the name of the variable
     * @param value - the integer value to set it to
     */
	public void setString(String variable, String value) {
		string_table.put(variable, value);
		return;
	}
	
	/**
	 * Set the instruction pointer to lineno.
	 * 
	 * @param lineno
	 */
	public void setIp(Integer lineno) {
		// TODO Auto-generated method stub
		ip = lineno;
		return;
	}

	public void pushToStack(Integer lineno) throws Exception{
		callstack.add(lineno);
		if (callstack.contains(lineno)) {
			return;
		}
		else {
			throw new Exception("GOSUB target could not be added to call stack");
		}
	}

	public void popFromStack() {
		int ip = callstack.remove(callstack.size() - 1);
		setIp(ip);
	}

		/**
	 * get the instruction pointer
	 * @return the instruction pointer
	 */
	public Integer getIp() {
		return ip;
	}
}
