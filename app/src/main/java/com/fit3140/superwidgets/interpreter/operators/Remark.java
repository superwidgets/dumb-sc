package com.fit3140.superwidgets.interpreter.operators;

import com.fit3140.superwidgets.interpreter.Context;

/**
 * a "no-op" OP.  Can be useful as a GOTO target at the end of a program, or to hold comments.
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 * @Modified_By Timothy Laird
 * @Last_Modified 08/05/2016
 */
public class Remark extends Op {

	public Remark(int line ) {
		super(line);// TODO Auto-generated constructor stub
	}

	@Override
	public String execute(Context context) {
		context.setIp(nextln);
		return "";
	}
}
