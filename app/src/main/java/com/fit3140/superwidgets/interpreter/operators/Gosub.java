package com.fit3140.superwidgets.interpreter.operators;

import com.fit3140.superwidgets.interpreter.Context;

/**
 * Created by james on 5/18/2016.
 */
public class Gosub extends Op {

    private String target; // either a string representing a numeric constant, or a variable name

    /**
     * create a GOTO op
     *
     * @param line - the line number of *this* op
     * @param targ - either a string literal of a number, or a variable name, which represents where we should GO TO
     */
    public Gosub(int line, String targ) {
        super(line);
        target = targ;

        return;
    }

    @Override
    public String execute(Context context) throws Exception {
        try {
            context.setIp(context.getValue(target));
            if (nextln == null) {
                throw new Exception("Gosub: No line to return to");
            }
            context.pushToStack(nextln);

            return "";
        }
        catch (Exception e) {
            throw new Exception("Gosub failed to execute\n" + e.getMessage());
        }
    }
}