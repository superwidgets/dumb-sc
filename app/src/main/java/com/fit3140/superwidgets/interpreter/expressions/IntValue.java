package com.fit3140.superwidgets.interpreter.expressions;

import com.fit3140.superwidgets.interpreter.Context;
import com.fit3140.superwidgets.interpreter.exceptions.ValueNotPresentException;

/**
 * Implements an expression consisting of a single value
 * 
 * @author Timothy Laird <tdlai2@student.monash.edu>
 *
 */
public class IntValue extends Expression {
    
	public IntValue(String left) {
		super(left, null);
	}

	@Override
	public int evaluate(Context context) throws ValueNotPresentException {
		return context.getValue(lhs);
	}
}