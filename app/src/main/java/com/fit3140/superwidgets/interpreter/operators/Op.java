package com.fit3140.superwidgets.interpreter.operators;

import com.fit3140.superwidgets.interpreter.Context;

/**
 * An abstract base class representing a JDUMBBASIC instruction.  They are generated from the source by the 
 * parser in OpMaker.
 * 
 * If you want to implement another operation, you'll need to:
 * 
 * Subclass this.  You will usually invoke the base class constructor, and you will need to implement the execute method.
 * add parsing instructions to OpMaker.
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 * @Modified_By Timothy Laird
 * @Last_Modified 08/05/2016
 *
 */
public abstract class  Op {
	
	protected Integer nextln;
	protected Integer lineno;
	
	/**
	 * set the line that comes after this one. 
	 * 
	 * This isn't in the constructor because you don't know what the next line is until parsing is complete
	 * @param next
	 */
	public void setNextLn(int next) {
		nextln=next;
	}
	
	/**
	 * gets the next line number as set by setnextln.
	 * @return the next line number
	 */
	public Integer getNextLn() {
		return nextln;
	}
	
	/**
	 * get the current line number.  Note that there is no corresponding setLineNo method as this
	 * is set in the constructor and is deliberately immutable.
	 * @return
	 */
	public Integer getLineNo() {
		return lineno;
	}
	/**
	 * base class constructor.  Invoked by the subclasses to handle line succession
	 * @param line
	 */
	public Op(int line) {
		lineno = line;
		nextln = null; // this can't be set at construction time because it is not known then
	}

	/**
	 * execute your instruction.  
	 * 
	 * The execute method is responsible for updating variables *and* the instruction pointer
	 * by invoking the appropriate methods in Context.
	 *  
	 * @param context - the program runtime state
     * @return String
	 * @throws Exception
     *
	 */
	public abstract String execute(Context context) throws Exception;
}
