package com.fit3140.superwidgets.dumbbasicscratch.Statements;

import java.util.Locale;

/**
 * Statement subclass representing a LET statement in DumbBasic
 *
 * @Author James Marmarou
 * @Last_Modified 12/05/2016
 *
 */
public class ReturnStatement extends Statement {

    public static final String TYPE = TYPES.RETURN;

    /**
     * Create a LET statement
     *
     */
    public ReturnStatement() {
    }

    @Override
    public String getType() {
        return TYPE;
    }

    /**
     * Returns a formatted string representing the statement
     *
     * @return String
     *
     */
    @Override
    public String toHtmlString() {
        String retVal;
        retVal = String.format(Locale.ENGLISH, "<b>%s</b>", TYPE);

        return retVal;
    }

    @Override
    public String toString() {
        String retVal;
        retVal = String.format(Locale.ENGLISH, "%s %s", getLineNo(), TYPE);
        return retVal;
    }
}
