package com.fit3140.superwidgets.dumbbasicscratch.Statements;

import android.support.annotation.Nullable;

import java.util.Locale;

/**
 * Statement subclass representing a LET statement in DumbBasic
 *
 * @Author James Marmarou
 * @Last_Modified 12/05/2016
 *
 */
public class IfGotoStatement extends Statement {

    public static final String TYPE = TYPES.IF_GOTO;
    public static final String[] OPS = new String[]{"=", ">"};

    private String target;
    private String arg1;
    private String op;
    private String arg2;

    /**
     * Create a LET statement
     *
     */
    public IfGotoStatement() {
        set(new String[]{"1", "100", "0", "100"});
    }

    /**
     * Get the type of operator used in the statement
     *
     * @return String
     *
     */
    public String[] get(){
        return new String[]{target, arg1, op, arg2};
    }

    /**
     * Get the type of operator used in the statement
     *
     * @return String
     *
     */
    public void set(String[] values){
        target = values[0].trim();
        arg1 = values[1].trim();
        op = values[2].trim();
        arg2 = values[3].trim().replace(" ", "");
    }

    @Override
    public String getType() {
        return TYPE;
    }

    /**
     * Returns a formatted string representing the statement
     *
     * @return String
     *
     */
    @Override
    public String toHtmlString() {
        String retVal;
        retVal = String.format(Locale.ENGLISH, "<b>%s</b> <i>%s</i> <b>IF</b> <i>%s</i> %s <i>%s</i>",
                TYPES.GOTO, target, arg1, OPS[Integer.parseInt(op)], arg2);

        return retVal;
    }

    @Override
    public String toString() {
        String retVal;
        retVal = String.format(Locale.ENGLISH, "%s IF %s %s %s %s %s",
                getLineNo(), arg1, OPS[Integer.parseInt(op)], arg2, TYPES.GOTO, target);

        return retVal;
    }
}
