package com.fit3140.superwidgets.dumbbasicscratch.Statements;

import android.support.annotation.Nullable;

import java.util.Locale;

/**
 * Statement subclass representing a LET statement in DumbBasic
 *
 * @Author James Marmarou
 * @Last_Modified 12/05/2016
 *
 */
public class LetStatement extends Statement {

    public static final String TYPE = TYPES.LET;
    public static final String[] OPS = new String[]{"+", "-"};

    private String varName;
    private String arg1;
    private String op;
    private @Nullable String arg2;

    /**
     * Create a LET statement
     *
     */
    public LetStatement() {
        set("x", "100", "0", "");
    }

    /**
     * Get the type of operator used in the statement
     *
     * @return String
     *
     */
    public String[] get(){
        return new String[]{varName, arg1, op, arg2};
    }

    /**
     * Get the type of operator used in the statement
     *
     * @return String
     *
     */
    public void set(String name, String arg_1, @Nullable String operator, @Nullable String arg_2){
        varName = name;
        arg1 = arg_1;
        op = operator;
        arg2 = arg_2;
    }

    /**
     * Get the type of operator used in the statement
     *
     * @return String
     *
     */
    public void set(String[] values){
        varName = values[0].trim();
        arg1 = values[1].trim();
        op = values[2].trim();
        arg2 = values[3].trim().replace(" ", "");
    }

    @Override
    public String getType() {
        return TYPE;
    }

    /**
     * Returns a formatted string representing the statement
     *
     * @return String
     *
     */
    @Override
    public String toHtmlString() {
        String retVal;
        retVal = String.format(Locale.ENGLISH, "<b>%s</b>  <i>%s</i>  =  <i>%s</i>", TYPE, varName, arg1);
        if (!arg2.equals("") && arg2 != null) {
            retVal = retVal.concat(String.format(Locale.ENGLISH, "  %s  <i>%s</i>", OPS[Integer.parseInt(op)], arg2));
        }

        return retVal;
    }

    @Override
    public String toString() {
        String retVal;
        retVal = String.format(Locale.ENGLISH, "%s %s %s = %s", getLineNo(), TYPE, varName, arg1);
        if (!arg2.equals("") && arg2 != null) {
            retVal = retVal.concat(String.format(Locale.ENGLISH, " %s %s", OPS[Integer.parseInt(op)], arg2));
        }

        return retVal;
    }
}
