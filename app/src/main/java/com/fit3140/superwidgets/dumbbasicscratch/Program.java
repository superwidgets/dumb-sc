package com.fit3140.superwidgets.dumbbasicscratch;

import android.util.Log;

import com.fit3140.superwidgets.dumbbasicscratch.Statements.LetStatement;
import com.fit3140.superwidgets.dumbbasicscratch.Statements.PrintStatement;
import com.fit3140.superwidgets.dumbbasicscratch.Statements.Statement;

import java.util.ArrayList;
import java.util.Collections;

/**
 * A representation of an editable DumbBasic program. Statements are stored in a collection
 * and can be accessed, modified, replaced, added and deleted.
 *
 * @Author Timothy Laird
 * @Last_Modified 12/05/2016
 *
 */
public class Program {

    private ArrayList<Statement> statements; // A collection of statements
    private String programName;
    private String description;
    private String author;
    private String emailAddress;

    public Program(){
        statements = new ArrayList<>();
        programName = "New Program";
        description = "";
        author = "";
        emailAddress = "";
    }

    /**
     * Accessor for the statement at the given index
     *
     * @param number, the index of the statement to be returned
     * @return String
     *
     */
    public Statement getStatementByLineNo(int number) {
        for (Statement s : statements) {
            if (Integer.parseInt(s.getLineNo()) == number) {
                return s;
            }
        }
        return null;
    }

    public Statement getStatement(int id) {
        for (Statement s : statements) {
            if (s.getViewId() == id) {
                return s;
            }
        }
        return null;
    }

    /**
     * Accessor for the statement at the given index
     *
     * @return String
     *
     */
    public String getName() {
        return programName;
    }

    public void setName(String value) {
        programName = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String value) {
        description = value;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String value) {
        author = value;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String value) {
        emailAddress = value;
    }

    /**
     * Adds a statement to the program
     *
     * @return boolean, true if successful, false otherwise
     *
     */
    public ArrayList<String> getTargets() {
        ArrayList<Integer> numbers = new ArrayList<>();
        ArrayList<String> variables = new ArrayList<>();

        for (Statement s : statements) {
            int number = Integer.parseInt(s.getLineNo());
            if (!numbers.contains(number)) { numbers.add(number); } // Add line numbers to targets

            if (s.getType().equals(Statement.TYPES.LET)) { // If it it s a LET statement
                LetStatement s_let = (LetStatement) s;
                if (!variables.contains(s_let.get()[0])) { variables.add(s_let.get()[0]); }  // Add variable names to the list
            }
        }
        Collections.sort(numbers);
        for (int num : numbers) {
            variables.add(Integer.toString(num));
        }
        return variables;
    }

    public void cancelAllEdits() {
        for (Statement s : statements) {
                s.setEditMode(false);
        }
    }

    /**
     * Adds a statement to the program
     *
     * @param s, the Statement object to be added
     * @return boolean, true if successful, false otherwise
     *
     */
    public boolean addStatement(Statement s) {
        try {
            s.setLineNo(Integer.toString(statements.size() + 1));
            statements.add(s);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * Replaces the statement at the given index with a new one
     *
     * @param index, the index of the statement to be replaced
     * @param s, the new statement
     * @return boolean, true if successful, false otherwise
     *
     */
    public boolean replaceStatement(int index, Statement s) {
        try {
            removeStatement(index);
            if (index + 1 > statements.size()) {
                statements.add(s);
            }
            else {
                statements.add(index, s);
            }
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * Removes a statement from the program
     *
     * @param index, the index of the statement to be removed
     * @return boolean, true if successful, false otherwise
     *
     */
    public boolean removeStatementByIndex(int index) {
        try {
            statements.remove(index);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    public boolean removeStatement(int id) {
        Statement s_old = null;
        int lineNo;
        for (Statement s : statements) {
            if (s.getViewId() == id) {
                s_old = s;
                break;
            }
        }
        if (s_old == null) {
            return false;
        }
        lineNo = Integer.parseInt(s_old.getLineNo());

        Log.d("ProgramDebug", String.format("removeStatement - Statement: %d", lineNo));
        statements.remove(s_old);

        for (int i = lineNo; i < statements.size() + 1; i++) {
            Log.d("ProgramDebug", String.format("removeStatement - Re-number statement %d to %d", i+1, i));
            getStatementByLineNo(i + 1).setLineNo(Integer.toString(i));
        }
        return true;
    }

    /**
     * @return int, the number of statements in the program
     *
     */
    public int size() {
        return statements.size();
    }

    /**
     * Compiles all statements into a DumbBasic script
     *
     * @return String
     *
     */
    public String toString() {
        String retVal = "";
        for (Statement line: statements) {
            retVal += line.toString() + "\n";
        }

        return retVal.substring(0, retVal.length() - 1);
    }
}
