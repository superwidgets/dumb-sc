package com.fit3140.superwidgets.dumbbasicscratch.Statements;

import android.support.annotation.Nullable;

import java.util.Locale;

/**
 * Statement subclass representing a LET statement in DumbBasic
 *
 * @Author James Marmarou
 * @Last_Modified 12/05/2016
 *
 */
public class PrintStatement extends Statement {

    private static final String TYPE = TYPES.PRINT;

    private String value;

    /**
     * Create a LET statement
     *
     */
    public PrintStatement() {
        set("0");
    }

    /**
     * Get the type of operator used in the statement
     *
     * @return String
     *
     */
    public String get(){
        return value;
    }

    /**
     * Get the type of operator used in the statement
     *
     * @return String
     *
     */
    public void set(String val){
        value = val;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    /**
     * Returns a formatted string representing the statement
     *
     * @return String
     *
     */
    @Override
    public String toHtmlString() {
        String retVal;
        retVal = String.format(Locale.ENGLISH, "<b>%s</b>  <i>%s</i>", TYPE, value);

        return retVal;
    }

    @Override
    public String toString() {
        String retVal;
        retVal = String.format(Locale.ENGLISH, "%s %s %s", getLineNo(), TYPE, value);

        return retVal;
    }
}
