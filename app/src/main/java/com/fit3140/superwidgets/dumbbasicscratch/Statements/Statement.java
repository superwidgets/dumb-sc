package com.fit3140.superwidgets.dumbbasicscratch.Statements;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * An abstract base class representing a statement in DumbBasic. This class is used to store data
 * from the UI and convert it into a string which can be read by the interpreter. It also contains a
 * static subclass which holds the types of operators that can be used.
 *
 * @Author Timothy Laird
 * @Last_Modified 12/05/2016
 *
 */
public abstract class Statement {

    private String lineNo; // field for storing the line number of a statement
    private int viewId; // the id of the associated view
    private boolean editMode = false;

    /**
     * Get the value of the line number field
     *
     * @return String
     *
     */
    public String getLineNo() {
        return lineNo;
    }

    /**
     * Set the value for the line number field
     *
     * @param value, value to set the line number to
     * @return Boolean, true if successful, false otherwise
     *
     */
    public Boolean setLineNo(String value) {
        lineNo = value;
        return true;
    }

    /**
     * Get the id of the associated view
     *
     * @return int
     *
     */
    public int getViewId() {
        return viewId;
    }

    /**
     * Set the id of the associated view. This is used to associate the statement with a view
     *
     * @param value, id of the associated view
     * @return Boolean, true if successful, false otherwise
     *
     */
    public Boolean setViewId(int value) {
        viewId = value;
        return true;
    }

    /**
     * Set the id of the associated view. This is used to associate the statement with a view
     *
     * @param value, id of the associated view
     * @return Boolean, true if successful, false otherwise
     *
     */
    public Boolean setEditMode(boolean value) {
        editMode = value;
        return true;
    }

    /**
     * Set the id of the associated view. This is used to associate the statement with a view
     *
     * @return Boolean, true if successful, false otherwise
     *
     */
    public Boolean isInEditMode() {
        return editMode;
    }

    /**
     * Abstract method which returns the statement as a string
     *
     * @return String
     *
     */
    @Override
    public abstract String toString();

    public abstract String toHtmlString();

    public abstract String getType();

    /**
     * A container class for the possible statement types. This class handles type specification
     * for statements, and provides a central source from which these values can be drawn
     *
     * @Author Timothy Laird
     * @Last_Modified 12/05/2016
     *
     */
    public static class TYPES {

        public static final String GOSUB = "GOSUB";
        public static final String GOTO = "GOTO";
        public static final String IF_GOTO = "IF";
        public static final String LET = "LET";
        public static final String PRINT = "PRINT";
        public static final String RETURN = "RETURN";

        public static final ArrayList<String> ALL = new ArrayList<>(Arrays.asList(
                GOSUB,
                GOTO,
                IF_GOTO,
                LET,
                PRINT,
                RETURN));
    }
}
