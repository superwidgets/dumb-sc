package com.fit3140.superwidgets.dumbbasicscratch;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * The top-level main class for the JDUMBBASIC interpreter
 *
 * See the README.md file for more information.
 *
 * @Author James Marmarou
 *
 */
public class Home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}
