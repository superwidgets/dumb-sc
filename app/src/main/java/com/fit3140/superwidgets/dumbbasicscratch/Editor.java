package com.fit3140.superwidgets.dumbbasicscratch;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;

import com.fit3140.superwidgets.dumbbasicscratch.Statements.GosubStatement;
import com.fit3140.superwidgets.dumbbasicscratch.Statements.GotoStatement;
import com.fit3140.superwidgets.dumbbasicscratch.Statements.IfGotoStatement;
import com.fit3140.superwidgets.dumbbasicscratch.Statements.LetStatement;
import com.fit3140.superwidgets.dumbbasicscratch.Statements.PrintStatement;
import com.fit3140.superwidgets.dumbbasicscratch.Statements.ReturnStatement;
import com.fit3140.superwidgets.dumbbasicscratch.Statements.Statement;
import com.fit3140.superwidgets.interpreter.exceptions.MyParseException;
import com.fit3140.superwidgets.ui.NavigationDrawerFragment;
import com.fit3140.superwidgets.ui.SlidingTabLayout;
import com.fit3140.superwidgets.ui.TabPagerAdapter;
import com.fit3140.superwidgets.dumbbasicscratch.Statements.Statement.TYPES;
import com.fit3140.superwidgets.interpreter.Interpreter;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.jmedeisis.draglinearlayout.DragLinearLayout;

/**
 * Activity responsible for editing a DumbBasic program
 *
 * @Author Timothy Laird
 * @Last_Modified 12/05/2016
 */
public class Editor extends AppCompatActivity {

    private Toolbar toolbar;
    private NavigationDrawerFragment drawerFragment;
    private DrawerLayout drawerLayout;
    private FloatingActionMenu actionMenu;
    private FloatingActionButton runButton;

    private ViewPager mPager;
    private SlidingTabLayout mTabs;

    private DragLinearLayout statementList; // will contain statements

    private Program program; // Container for Statements
    private Interpreter interpreter;

    /**
     * Creates the activity and initialises the required variables and UI elements
     *
     * @param savedInstanceState description
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("EditorDebug", "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_ui);

        // Initialize program
        program = new Program();
        PrintStatement s = new PrintStatement();
        s.set(getResources().getString(R.string.hello_world));
        program.addStatement(s);

        // Get reference to the root layout views
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Set up toolbar
        toolbar = (Toolbar) findViewById(R.id.appBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(program.getName());

        // Set up action menu
        actionMenu = (FloatingActionMenu) drawerLayout.findViewById(R.id.fam);
        actionMenu.setClosedOnTouchOutside(true);
        actionMenu.findViewById(R.id.fab_gosub).setOnClickListener(addLineButtonListener);
        actionMenu.findViewById(R.id.fab_goto).setOnClickListener(addLineButtonListener);
        actionMenu.findViewById(R.id.fab_if_goto).setOnClickListener(addLineButtonListener);
        actionMenu.findViewById(R.id.fab_let).setOnClickListener(addLineButtonListener);
        actionMenu.findViewById(R.id.fab_print).setOnClickListener(addLineButtonListener);
        actionMenu.findViewById(R.id.fab_return).setOnClickListener(addLineButtonListener);

        // Set up run action button
        runButton = (FloatingActionButton) drawerLayout.findViewById(R.id.fab_run);
        runButton.setOnClickListener(runButtonListener);
        runButton.hide(false);

        // Set up navigation drawer
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(drawerLayout, toolbar, R.id.fragment_navigation_drawer);

        // Set up sliding tab view
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new TabPagerAdapter(getSupportFragmentManager(), getResources().getStringArray(R.array.tabs)));
        mPager.setCurrentItem(1);
        mTabs = (SlidingTabLayout) drawerLayout.findViewById(R.id.tabs);
        mTabs.setDistributeEvenly(true);
        mTabs.setFloatingActionMenu(actionMenu);
        mTabs.setFloatingActionButton(runButton);
        mTabs.setViewPager(mPager);

        // Get an interpreter
        interpreter = new Interpreter();
    }

    /**
     * Refreshes the statement table so that it displays the current state of the program
     */
    public void refreshStatements() {
        Log.d("EditorDebug", "refreshStatements");
        // Find the linear layout that holds all the statements so that we can remove them all
        TabPagerAdapter a = (TabPagerAdapter) mPager.getAdapter();
        View v = a.getRegisteredFragment(1).getView();
        statementList = ((DragLinearLayout) v.findViewById(R.id.statement_list));
        statementList.removeAllViews();

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int index = 0; index < program.size(); ++index) {
            makeStatementView(inflater, index, statementList);
        }
    }

    /**
     * Inflates a statement view and assigns values to the fields
     */
    private void makeStatementView(LayoutInflater inflater, int index) {
        makeStatementView(inflater, index, statementList);
    }

    /**
     * Inflates a statement view and assigns values to the fields
     */
    public void makeStatementView(LayoutInflater inflater, int index, DragLinearLayout list) {
        Log.d("EditorDebug", String.format("MakeStatementView - Statement: %d", index + 1));

        Statement s = program.getStatementByLineNo(index + 1);
        CardView s_view;

        if (s.isInEditMode()) {
            s_view = setUpEditView(inflater, s);
        }
        else {
            s_view = setUpDisplayView(inflater, s);
        }

        // Give the new view a view ID
        int id = View.generateViewId();
        s.setViewId(id);
        s_view.setId(id);

        // Set generic view properties
        s_view.setCardBackgroundColor(getStatementViewBackgroundColor(s.getType()));
        s_view.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)); // Stops the cards from expanding

        Log.d("EditorDebug", "makeStatementGUI - Adding view to list");
        list.addView(s_view, index);
        list.setViewDraggable(s_view, s_view);
    }

    private CardView setUpDisplayView(LayoutInflater inflater, Statement s) {
        CardView s_view = (CardView) inflater.inflate(R.layout.view_statement, null);

        // Allow the card to be editable unless it's a RETURN card
        if (!s.getType().equals(TYPES.RETURN)) {
            s_view.setOnClickListener(editStatementListener);
        }

        Log.d("EditorDebug", "makeStatementGUI - Retrieving values from statement");
        // Get child views and give them values from the statement they represent
        TextView lineNo = (TextView) s_view.findViewById(R.id.number);
        lineNo.setText(s.getLineNo());

        TextView expression = (TextView) s_view.findViewById(R.id.expression);
        expression.setText(Html.fromHtml(s.toHtmlString()));

        Log.d("EditorDebug", "makeStatementGUI - Registering delete button listener");
        ImageView deleteButton = (ImageView) s_view.findViewById(R.id.delete);
        deleteButton.setOnClickListener(deleteButtonListener);

        return s_view;
    }

    private CardView setUpEditView(LayoutInflater inflater, Statement s) {
        CardView s_view;
        String value = null;
        String[] values;
        ImageView save;
        Spinner op;

        switch (s.getType()) {
            case TYPES.GOSUB:
                // Get previous target
                value = ((GosubStatement) s).get();

            case TYPES.GOTO:
                if (value == null) { // Get previous target if not GOSUB
                    value = ((GotoStatement) s).get();
                }

                // Inflate appropriate layout and get values from statement
                s_view = (CardView) inflater.inflate(R.layout.edit_goto, null);

                // Get child views and give them values from the statement they represent
                ((TextView) s_view.findViewById(R.id.number)).setText(s.getLineNo()); // Line Number

                // Set up spinner with available targets
                op = (Spinner) s_view.findViewById(R.id.value_1);
                op.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,
                        program.getTargets()));
                op.setSelection(program.getTargets().indexOf(value));

                // Register save button listener
                save = (ImageView) s_view.findViewById(R.id.save);
                save.setOnClickListener(saveGotoListener);
                return s_view;

            case TYPES.IF_GOTO:
                // Get previous values
                values = ((IfGotoStatement) s).get();

                // Inflate appropriate layout and get values from statement
                s_view = (CardView) inflater.inflate(R.layout.edit_ifgoto, null);

                Log.d("EditorDebug", "makeStatementGUI - Retrieving values from statement");
                // Get child views and give them values from the statement they represent
                ((TextView) s_view.findViewById(R.id.number)).setText(s.getLineNo()); // Line Number
                ((EditText) s_view.findViewById(R.id.value_2)).setText(values[1]); // Argument 1
                ((EditText) s_view.findViewById(R.id.value_4)).setText(values[3]); // Argument 2

                // Set up target spinner with available targets
                op = (Spinner) s_view.findViewById(R.id.value_1);
                op.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,
                        program.getTargets()));
                op.setSelection(program.getTargets().indexOf(values[0]));

                // Set up ops spinner with available ops
                op = (Spinner) s_view.findViewById(R.id.value_3);
                op.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,
                        ((IfGotoStatement) s).OPS));
                op.setSelection(Integer.parseInt(values[2]));

                // Register save button listener
                save = (ImageView) s_view.findViewById(R.id.save);
                save.setOnClickListener(saveIfGotoListener);
                return s_view;

            case TYPES.LET:
                // Get previous values
                values = ((LetStatement) s).get();

                // Inflate appropriate layout and get values from statement
                s_view = (CardView) inflater.inflate(R.layout.edit_let, null);

                Log.d("EditorDebug", "makeStatementGUI - Retrieving values from statement");
                // Get child views and give them values from the statement they represent
                ((TextView) s_view.findViewById(R.id.number)).setText(s.getLineNo()); // Line Number
                ((EditText) s_view.findViewById(R.id.value_1)).setText(values[0]); // Variable name
                ((EditText) s_view.findViewById(R.id.value_2)).setText(values[1]); // Argument 1
                ((EditText) s_view.findViewById(R.id.value_4)).setText(values[3]); // Argument 2

                // Set up spinner with available ops
                op = (Spinner) s_view.findViewById(R.id.value_3);
                op.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,
                        ((LetStatement) s).OPS));
                op.setSelection(Integer.parseInt(values[2]));

                // Register save button listener
                save = (ImageView) s_view.findViewById(R.id.save);
                save.setOnClickListener(saveLetListener);
                return s_view;

            case TYPES.PRINT:
                // Get previous value
                value = ((PrintStatement) s).get();

                // Inflate appropriate layout and get values from statement
                s_view = (CardView) inflater.inflate(R.layout.edit_print, null);

                Log.d("EditorDebug", "makeStatementGUI - Retrieving values from statement");
                // Get child views and give them values from the statement they represent
                ((TextView) s_view.findViewById(R.id.number)).setText(s.getLineNo()); // Line Number
                ((EditText) s_view.findViewById(R.id.value_1)).setText(value); // Print value

                // Register save button listener
                save = (ImageView) s_view.findViewById(R.id.save);
                save.setOnClickListener(savePrintListener);
                return s_view;
        }
        return null;
    }

    /**
     * Listener class which adds a new, blank, statement to the program
     */
    public OnClickListener addLineButtonListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d("EditorDebug", "Event - Add Line button clicked");

            switch (v.getId()) {
                case R.id.fab_goto:
                    program.addStatement(new GotoStatement()); break;
                case R.id.fab_gosub:
                    program.addStatement(new GosubStatement()); break;
                case R.id.fab_if_goto:
                    program.addStatement(new IfGotoStatement()); break;
                case R.id.fab_let:
                    program.addStatement(new LetStatement()); break;
                case R.id.fab_print:
                    program.addStatement(new PrintStatement()); break;
                case R.id.fab_return:
                    program.addStatement(new ReturnStatement()); break;
            }
            // Hide the keyboard when if it's showing
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mPager.getWindowToken(), 0);

            // Cancel existing edits
            program.cancelAllEdits();

            refreshStatements();
        }
    };

    /**
     * Listener class which adds a new, blank, statement to the program
     */
    public OnClickListener editStatementListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d("EditorDebug", "Event - edit view");

            // Cancel existing edits
            program.cancelAllEdits();

            // Set statement to edit mode
            program.getStatement(v.getId()).setEditMode(true);

            actionMenu.close(true);

            refreshStatements();
        }
    };

    /**
     * Listener class which runs the program
     */
    public OnClickListener runButtonListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d("EditorDebug", "Event - Run button clicked");
            refreshStatements();

            String output;
            if (program.size() != 0) {
                interpreter.load(program.toString());
                try {
                    output = interpreter.run();
                } catch (MyParseException e) {
                    output = e.toString() + "\nRun Error: Failed to run program\n" + e.getMessage();
                }
            }
            else { // no statements
                output = "Nothing to run";
            }

            Log.i("ProgramOutput", "\n" + output);
            TabPagerAdapter a = (TabPagerAdapter) mPager.getAdapter();
            v = a.getRegisteredFragment(2).getView();
            TextView o = ((TextView) v.findViewById(R.id.output));
            o.setText(output);
        }
    };

    /**
     * Listener class which deletes the associated statement from the program
     */
    public OnClickListener deleteButtonListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d("EditorDebug", String.format("Event - Delete button clicked for view: %d", v.getId()));
            // get all necessary GUI components
            CardView s_view = (CardView) v.getParent().getParent();

            // statement needs to be removed from the program
            program.removeStatement(s_view.getId());

            // Remove the row
            refreshStatements();
        }
    };

    public OnClickListener saveGotoListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            CardView s_view = (CardView) v.getParent().getParent();
            Statement statement = program.getStatement(s_view.getId());

            if (statement.getType().equals(TYPES.GOSUB)) {
                GosubStatement s = (GosubStatement) statement;
                s.set(((Spinner) s_view.findViewById(R.id.value_1)).getSelectedItem().toString());
                s.setEditMode(false);
            }
            else {
                GotoStatement s = (GotoStatement) statement;
                s.set(((Spinner) s_view.findViewById(R.id.value_1)).getSelectedItem().toString());
                s.setEditMode(false);
            }
            // Hide the keyboard when if it's showing
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mPager.getWindowToken(), 0);
            refreshStatements();
        }
    };

    public OnClickListener saveIfGotoListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            CardView s_view = (CardView) v.getParent().getParent();
            IfGotoStatement s = (IfGotoStatement) program.getStatement(s_view.getId());
            String[] values = new String[4];

            values[0] = ((Spinner) s_view.findViewById(R.id.value_1)).getSelectedItem().toString();
            values[1] = ((EditText) s_view.findViewById(R.id.value_2)).getText().toString();
            values[2] = Integer.toString(((Spinner) s_view.findViewById(R.id.value_3)).getSelectedItemPosition());
            values[3] = ((EditText) s_view.findViewById(R.id.value_4)).getText().toString();

            s.set(values);
            s.setEditMode(false);

            // Hide the keyboard when if it's showing
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mPager.getWindowToken(), 0);
            refreshStatements();
        }
    };

    public OnClickListener saveLetListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            CardView s_view = (CardView) v.getParent().getParent();
            LetStatement s = (LetStatement) program.getStatement(s_view.getId());
            String[] values = new String[4];

            values[0] = ((EditText) s_view.findViewById(R.id.value_1)).getText().toString();
            values[1] = ((EditText) s_view.findViewById(R.id.value_2)).getText().toString();
            values[2] = Integer.toString(((Spinner) s_view.findViewById(R.id.value_3)).getSelectedItemPosition());
            values[3] = ((EditText) s_view.findViewById(R.id.value_4)).getText().toString();

            s.set(values);
            s.setEditMode(false);

            // Hide the keyboard when if it's showing
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mPager.getWindowToken(), 0);
            refreshStatements();
        }
    };

    public OnClickListener savePrintListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            CardView s_view = (CardView) v.getParent().getParent();
            PrintStatement s = (PrintStatement) program.getStatement(s_view.getId());

            s.set(((EditText) s_view.findViewById(R.id.value_1)).getText().toString());
            s.setEditMode(false);

            // Hide the keyboard when if it's showing
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mPager.getWindowToken(), 0);
            refreshStatements();
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private int getStatementViewBackgroundColor(String type) {
        switch (type) {
            case TYPES.GOSUB:
                return ContextCompat.getColor(getBaseContext(), R.color.light_teal);
            case TYPES.GOTO:
                return ContextCompat.getColor(getBaseContext(), R.color.light_green);
            case TYPES.IF_GOTO:
                return ContextCompat.getColor(getBaseContext(), R.color.light_purple);
            case TYPES.LET:
                return ContextCompat.getColor(getBaseContext(), R.color.light_red);
            case TYPES.PRINT:
                return ContextCompat.getColor(getBaseContext(), R.color.light_blue_grey);
            case TYPES.RETURN:
                return ContextCompat.getColor(getBaseContext(), R.color.light_orange);
            default:
                return 0;
        }
    }

    public Program getProgram() {
        return program;
    }
}