package com.fit3140.superwidgets.dumbbasicscratch.Statements;

import java.util.Locale;

/**
 * Statement subclass representing a LET statement in DumbBasic
 *
 * @Author James Marmarou
 * @Last_Modified 12/05/2016
 *
 */
public class GotoStatement extends Statement {

    public static final String TYPE = TYPES.GOTO;

    private String target;

    /**
     * Create a LET statement
     *
     */
    public GotoStatement() {
        set("1");
    }

    /**
     * Get the type of operator used in the statement
     *
     * @return String
     *
     */
    public String get(){
        return target;
    }

    /**
     * Get the type of operator used in the statement
     *
     * @return String
     *
     */
    public void set(String value){
        target = value;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    /**
     * Returns a formatted string representing the statement
     *
     * @return String
     *
     */
    @Override
    public String toHtmlString() {
        String retVal;
        retVal = String.format(Locale.ENGLISH, "<b>%s</b>  <i>%s</i>", TYPE, target);

        return retVal;
    }

    @Override
    public String toString() {
        String retVal;
        retVal = String.format(Locale.ENGLISH, "%s %s %s", getLineNo(), TYPE, target);

        return retVal;
    }
}
