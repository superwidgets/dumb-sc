package com.fit3140.superwidgets.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.fit3140.superwidgets.dumbbasicscratch.Editor;
import com.fit3140.superwidgets.dumbbasicscratch.Program;
import com.fit3140.superwidgets.dumbbasicscratch.R;
import com.fit3140.superwidgets.dumbbasicscratch.Statements.Statement;
import com.jmedeisis.draglinearlayout.DragLinearLayout;

/**
 * Created by Tim on 24/05/2016.
 */

public class PageFragment extends Fragment {
    private static final String POSITION = "position";

    public static PageFragment getInstance(int position) {
        Bundle args = new Bundle();
        args.putInt(POSITION, position);

        PageFragment fragment = new PageFragment();
        fragment.setArguments(args);

        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();

        if (bundle!=null) {
            switch (bundle.getInt(POSITION)) {
                case 0:
                    return inflateDetailsPage(inflater, container);
                case 1:
                    return inflateEditorPage(inflater, container);
                case 2:
                    return inflateOutputPage(inflater, container);
            }
        }
        return null;
    }

    private View inflateDetailsPage(LayoutInflater inflater, @Nullable ViewGroup container) {
        View layout = inflater.inflate(R.layout.layout_details, container, false);

        EditText name = (EditText) layout.findViewById(R.id.details_name);
        name.setOnFocusChangeListener(metadataListener);
        name.setText(((Editor)getActivity()).getProgram().getName());

        EditText desc = (EditText) layout.findViewById(R.id.details_description);
        desc.setOnFocusChangeListener(metadataListener);
        desc.setText(((Editor)getActivity()).getProgram().getDescription());

        EditText auth = (EditText) layout.findViewById(R.id.details_author);
        auth.setOnFocusChangeListener(metadataListener);
        auth.setText(((Editor)getActivity()).getProgram().getAuthor());

        EditText email = (EditText) layout.findViewById(R.id.details_email);
        email.setOnFocusChangeListener(metadataListener);
        email.setText(((Editor)getActivity()).getProgram().getEmailAddress());

        return layout;
    }

    private View inflateEditorPage(LayoutInflater inflater, @Nullable ViewGroup container) {
        View layout = inflater.inflate(R.layout.layout_editor, container, false);
        DragLinearLayout v = (DragLinearLayout) layout.findViewById(R.id.statement_list);
        v.setContainerScrollView((ScrollView) v.getParent());

        // Swap statements in program
        v.setOnViewSwapListener(new DragLinearLayout.OnViewSwapListener() {
            @Override
            public void onSwap(View firstView, int firstPosition,
                               View secondView, int secondPosition) {
                Editor e = (Editor) getActivity();
                Program program = e.getProgram();
                Statement s;

                // Replace both statements at their new index
                if (firstView != null) {
                    s = program.getStatement(firstView.getId());
                    s.setLineNo(Integer.toString(secondPosition + 1));
                    ((TextView) firstView.findViewById(R.id.number)).setText(s.getLineNo());
                    // program.replaceStatement(secondPosition, s);
                }
                if (secondView != null) { // make sure there is a second view
                    s = program.getStatement(secondView.getId());
                    s.setLineNo(Integer.toString(firstPosition + 1));
                    ((TextView) secondView.findViewById(R.id.number)).setText(s.getLineNo());
                    // program.replaceStatement(firstPosition, s);
                }
            }
        });

        ((Editor)getActivity()).makeStatementView(inflater, 0, v);

        return layout;
    }

    private View inflateOutputPage(LayoutInflater inflater, @Nullable ViewGroup container) {
        View layout = inflater.inflate(R.layout.layout_output, container, false);
        return layout;
    }

    private View.OnFocusChangeListener metadataListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            EditText field = (EditText) v;
            switch (field.getId()) {
                case R.id.details_name:
                    ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle((field).getText().toString());
                    ((Editor)getActivity()).getProgram().setName(field.getText().toString());

                case R.id.details_description:
                    ((Editor)getActivity()).getProgram().setDescription(field.getText().toString());

                case R.id.details_author:
                    ((Editor)getActivity()).getProgram().setAuthor(field.getText().toString());

                case R.id.details_email:
                    ((Editor)getActivity()).getProgram().setEmailAddress(field.getText().toString());
            }
        }
    };
}
